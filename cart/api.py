from .models import ShoppingCart, OrderHistory
from rest_framework import viewsets, permissions
from .serializers import ShoppingCartSerializer, OrderHistorySerializer

# ShoppingCart Viewset
class ShoppingCartViewSet(viewsets.ModelViewSet):
    queryset = ShoppingCart.objects.all()
    permission_classes = [
        permissions.AllowAny
    ]
    serializer_class = ShoppingCartSerializer

# OrderHistory Viewset
class OrderHistoryViewSet(viewsets.ModelViewSet):
    queryset = OrderHistory.objects.all()
    permission_classes = [
        permissions.AllowAny
    ]
    serializer_class = OrderHistorySerializer