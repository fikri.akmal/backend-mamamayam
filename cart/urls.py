from rest_framework import routers
from .api import ShoppingCartViewSet, OrderHistoryViewSet
from django.urls import include, path
from . import views

router = routers.DefaultRouter()
router.register("shop", ShoppingCartViewSet, "shoppingCart")
router.register("history", OrderHistoryViewSet, "orderHistory")

urlpatterns = [
    path('user-order-history/', views.OrderHistoryList.as_view(), name = "user-order-history"),
    path('user-order-history-with-barang-object/', views.OrderHistoryListWithBarangObject.as_view(), name = "user-order-history-with-barang-object")
]

urlpatterns += router.urls