from django.shortcuts import render
from rest_framework import generics
from django.core import serializers
from rest_framework.views import APIView
from users.models import CustomUser
from .models import OrderHistory
from .serializers import OrderHistorySerializer, OrderHistorySerializerWithBarangObject

# Create your views here.

class OrderHistoryList(generics.ListAPIView):
    serializer_class = OrderHistorySerializer

    def get_queryset(self):
        username = self.request.GET.get("username")
        user_obj = CustomUser.objects.get(username=username)

        if username is not None:
            return OrderHistory.objects.filter(username=user_obj.id)

        return OrderHistory.objects.all()


class OrderHistoryListWithBarangObject(generics.ListAPIView):
    serializer_class = OrderHistorySerializerWithBarangObject

    def get_queryset(self):
        username = self.request.GET.get("username")
        user_obj = CustomUser.objects.get(username=username)

        if username is not None:
            return OrderHistory.objects.filter(username=user_obj.id)

        return OrderHistory.objects.all()