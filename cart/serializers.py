from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from .models import ShoppingCart, OrderHistory
from product.serializers import ProductSerializer

class ShoppingCartSerializer(ModelSerializer):
    class Meta:
        model = ShoppingCart
        fields = '__all__'
        

class OrderHistorySerializer(ModelSerializer):
    tanggal = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")
    class Meta:
        model = OrderHistory
        fields = '__all__'


class OrderHistorySerializerWithBarangObject(ModelSerializer):
    tanggal = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")
    listBarang = ProductSerializer(read_only=True, many=True)
    class Meta:
        model = OrderHistory
        fields = '__all__'