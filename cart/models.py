from django.db import models

# Create your models here.
class ShoppingCart(models.Model):
    username = models.ForeignKey('users.CustomUser', on_delete= models.CASCADE)
    listBarang = models.ManyToManyField('product.Product', blank=True)

class OrderHistory(models.Model):
    idOrder = models.BigAutoField(primary_key=True)
    username = models.ForeignKey('users.CustomUser', on_delete= models.CASCADE)
    listBarang = models.ManyToManyField('product.Product', blank=True)
    tanggal = models.DateTimeField()
    totalHarga = models.IntegerField()
    status = models.CharField(max_length= 200)

    def __str__(self):
        return "Order " + str(self.idOrder)