from django.urls import include, path
from .views import get_user_role, get_user_id

urlpatterns = [
    path('auth/', include('rest_auth.urls')),    
    path('auth/register/', include('rest_auth.registration.urls')),
    path('auth/check-role', get_user_role, name="get_user_role"),
    path('auth/get-user-id', get_user_id, name="get_user_id")
]