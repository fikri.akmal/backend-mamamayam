from django.contrib.auth.models import User
from django.shortcuts import render
from rest_framework.decorators import api_view
from .models import CustomUser
from rest_framework.response import Response
from .serializers import UserSerializer

# Create your views here.

@api_view(['POST'])
def get_user_role(request):
    if request.method == "POST":
        username = request.data.get('username')
        user_obj = CustomUser.objects.get(username=username)
        isAdmin = True if user_obj.is_staff else False

        return Response({
            "username": username,
            "isAdmin": isAdmin
        })

@api_view(['POST'])
def get_user_id(request):
    if request.method == "POST":
        username = request.data.get('username')
        user_obj = CustomUser.objects.get(username=username)
        user_id = user_obj.id

        return Response({
            "username": username,
            "userId" : user_id
        })