from django.db.models.base import Model
from rest_framework.serializers import ModelSerializer
from .models import Product


class ProductSerializer (ModelSerializer):
    class Meta:
        model = Product
        fields = "__all__"