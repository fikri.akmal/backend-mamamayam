from django.db import models
from django.contrib.postgres.fields import ArrayField

# Create your models here.
class Product(models.Model):
    idProduct = models.BigAutoField(primary_key=True)
    namaProduct = models.CharField(max_length=60)
    harga = models.IntegerField()
    tipeMakanan = models.CharField( max_length= 200, null=True)
    tags = ArrayField(models.CharField(max_length=20))
    diskon = models.IntegerField(default=0)
    imageUrl = models.CharField(max_length=300, default="https://cdn.discordapp.com/attachments/861914096810197042/861914133342978078/index.jpg")
    description = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return f'{self.namaProduct}'

    def __repr__(self):
        return f'{self.namaProduct}'