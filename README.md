# Back End Repository of Mamam Ayam Website
This is a part time project to learn about React.js in general

## Developed By:
### - Putu Wigunarta (Computer Science'19 of Universitas Indonesia)
### - Fikri Akmal (Computer Science'19 of Universitas Indonesia)

## Back End:
[![pipeline status](https://gitlab.com/fikri.akmal/frontend-mamamayam/badges/master/pipeline.svg)](https://gitlab.com/fikri.akmal/backend-mamamayam/-/commits/master)
## Front End:
[![pipeline status](https://gitlab.com/fikri.akmal/frontend-mamamayam/badges/master/pipeline.svg)](https://gitlab.com/fikri.akmal/frontend-mamamayam/-/commits/master)